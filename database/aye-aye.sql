-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mar 07 Septembre 2021 à 11:27
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `aye-aye`
--

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `admin`
--

INSERT INTO `admin` (`admin_id`, `login`, `password`) VALUES
(3, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997');

-- --------------------------------------------------------

--
-- Structure de la table `booking`
--

CREATE TABLE `booking` (
  `booking_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `motel_id` int(11) NOT NULL,
  `adult_nb` decimal(2,0) NOT NULL,
  `child_nb` decimal(2,0) NOT NULL,
  `baby_nb` decimal(2,0) NOT NULL,
  `price` decimal(9,0) NOT NULL,
  `duration` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `booking`
--

INSERT INTO `booking` (`booking_id`, `client_id`, `offer_id`, `motel_id`, `adult_nb`, `child_nb`, `baby_nb`, `price`, `duration`) VALUES
(1, 1, 1, 1, '1', '0', '0', '70000', 3),
(2, 2, 2, 2, '2', '0', '1', '100000', 5),
(3, 3, 5, 2, '2', '1', '1', '300000', 4),
(4, 3, 4, 1, '1', '5', '0', '400000', 7),
(5, 2, 3, 2, '1', '0', '0', '970000', 6);

-- --------------------------------------------------------

--
-- Structure de la table `car`
--

CREATE TABLE `car` (
  `car_id` varchar(7) COLLATE utf8_bin NOT NULL,
  `driver` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `car`
--

INSERT INTO `car` (`car_id`, `driver`) VALUES
('1', 'Dominic Torrento'),
('2', 'Paul Walker'),
('3', 'Mr Deze'),
('4', 'Transporteur');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `client_id` int(11) NOT NULL,
  `first_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `last_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `birthday` date NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`client_id`, `first_name`, `last_name`, `birthday`, `email`, `password`) VALUES
(1, 'Rasolofoniaiana', 'Fy', '1989-12-12', 'Rfy@gmail.com', 'bb6280d7b988aaee8e18cdede6fb53220cd0a186'),
(2, 'Bodo', '', '1966-07-21', 'Bodo@gmail.com', '01caf49116fadf1a06579a241790fc724255f91a'),
(3, 'Kylian', 'Mbappé ', '1998-12-20', 'Kmbappe@gmail.com', '613af4bf3b16709235e2c21e9a9d7e135697b75d');

-- --------------------------------------------------------

--
-- Structure de la table `experience`
--

CREATE TABLE `experience` (
  `experience_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` longtext COLLATE utf8_bin NOT NULL,
  `car_id` varchar(7) COLLATE utf8_bin NOT NULL,
  `max_traveler` int(10) UNSIGNED NOT NULL COMMENT 'Nombre des voyageurs max',
  `first_guide` int(11) NOT NULL COMMENT 'ID du premier guide touristique',
  `second_guide_id` int(11) NOT NULL COMMENT 'ID du deuxième guide touristique'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `experience`
--

INSERT INTO `experience` (`experience_id`, `name`, `category_id`, `description`, `car_id`, `max_traveler`, `first_guide`, `second_guide_id`) VALUES
(1, 'Hiantsinanana', 1, 'Decouvrez la beauté du côte est de Madagasikara en parcourant ses divers parcs nationals, villages touristiques, plages et touts les autres beautés qu\'offre cette parcours. Vivez des aventures et des expériences uniques les unes après les autres, découvrez des espèces animales est végetales uniques ...', '1', 24, 1, 2),
(2, 'Hianatsimo', 2, 'Le sud de Madagasikara, un véritable trésor de la nature qui contient des paradis uniques et distincts en passant par des parcs nationales qui contient des jacuzzi naturel, des espèces animales et végetales uniques, des paysages rocheuses avec toutes les formes, des oasis aux milieux des deserts, des plages avec du sable très fin ou des surfs à gogo, ...', '3', 24, 3, 4);

-- --------------------------------------------------------

--
-- Structure de la table `experience_category`
--

CREATE TABLE `experience_category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `experience_category`
--

INSERT INTO `experience_category` (`category_id`, `name`) VALUES
(1, 'En plein air'),
(2, 'hebergement hotel');

-- --------------------------------------------------------

--
-- Structure de la table `experience_reservation`
--

CREATE TABLE `experience_reservation` (
  `experience_reservation_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `experience_id` int(11) NOT NULL,
  `adult_nb` decimal(2,0) NOT NULL,
  `child_nb` decimal(2,0) NOT NULL,
  `baby_nb` decimal(2,0) NOT NULL,
  `price` decimal(9,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `experience_reservation`
--

INSERT INTO `experience_reservation` (`experience_reservation_id`, `client_id`, `experience_id`, `adult_nb`, `child_nb`, `baby_nb`, `price`) VALUES
(1, 2, 1, '2', '2', '3', '200000'),
(2, 2, 2, '1', '0', '0', '80000');

-- --------------------------------------------------------

--
-- Structure de la table `guide`
--

CREATE TABLE `guide` (
  `guide_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `guide`
--

INSERT INTO `guide` (`guide_id`, `name`) VALUES
(1, 'Felisto Smoak'),
(2, 'Armendine Safidy'),
(3, 'Mr Renez'),
(4, 'Ludovic');

-- --------------------------------------------------------

--
-- Structure de la table `motel`
--

CREATE TABLE `motel` (
  `motel_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  `phone` decimal(9,0) UNSIGNED NOT NULL COMMENT 'Numero tel sans +261',
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `bank_account` decimal(12,0) UNSIGNED NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `location` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `motel`
--

INSERT INTO `motel` (`motel_id`, `name`, `phone`, `email`, `bank_account`, `password`, `location`) VALUES
(1, 'Zahamotel', '206223709', 'zaha@gmail.com', '200350002423', 'fc8ca85510f157520d30d691dd367e3bf49ef881', 'Ave D\' Amborovy, Mahajanga 401'),
(2, 'Motel d\'Isalo', '320262123', 'Misalo@gmail.com', '411200315698', 'b2d1b3f0830789d8ed33b6441108c81f6176aff6', 'RN7, Ranohira'),
(3, 'L\'Heure Bleue', '320220361', 'Hblue@gmail.com', '411225003689', '0cc10063626b2dca6bb0fd002aff50adb55d0aa9', 'Plage de Madirokely BP 372, Madirokely, Nosy Be 207 Madagascar'),
(4, 'Motel d\'Antananarivo Anosy', '202267083', 'Manosy@gmail.com', '452103679632', '06bc43040143361c0f407f8ef63551b939a3a158', 'Route d\'Arivonimamo, Anosy, Antananarivo 101'),
(5, 'Hôtel Benjamin', '320240813', 'Hbejamain@gmail.com', '200523547896', '9aa7a7c64a883beba93e66271ddccd4fd364000c', 'Ambatoloaka, Nosy Be 207, Madagascar, Lot II 02 A 067 BP268, Ambatoloaka');

-- --------------------------------------------------------

--
-- Structure de la table `offer`
--

CREATE TABLE `offer` (
  `offer_id` int(11) NOT NULL,
  `motel_id` int(11) NOT NULL,
  `room_category_id` int(11) NOT NULL,
  `max_adult` decimal(2,0) UNSIGNED NOT NULL COMMENT 'Nombre max des personnes adultes',
  `max_child` decimal(2,0) UNSIGNED NOT NULL COMMENT 'Nombre max des enfants',
  `max_baby` decimal(2,0) UNSIGNED NOT NULL COMMENT 'Nombre max des enfants',
  `price` decimal(9,2) UNSIGNED NOT NULL COMMENT 'Prix sejour',
  `have_kitchen` bit(1) NOT NULL COMMENT '1 si la chambre à un cuisine\n0 sinon',
  `is_available` bit(1) NOT NULL COMMENT 'Liste des offres identiques que l''hotel a',
  `description` longtext COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `offer`
--

INSERT INTO `offer` (`offer_id`, `motel_id`, `room_category_id`, `max_adult`, `max_child`, `max_baby`, `price`, `have_kitchen`, `is_available`, `description`) VALUES
(1, 1, 2, '2', '0', '0', '60101.00', b'1', b'1', 'Confortable, spacieux, bonne emplacement, un très bon Rapport Qualité Prix. Wifi gratuit, Service complet de blanchisserie, Pas de piscine, Pas de centre de remise en forme, dispose de Bar et de restaurant'),
(2, 4, 1, '2', '0', '0', '54222.00', b'0', b'1', 'Hotel parfait+ chambre a la malgache'),
(3, 5, 3, '2', '2', '0', '198925.00', b'0', b'1', 'Un personnel très accueillant et vraiment disponible. Des bungalows propres et bien équipés dans un grand jardin et disposés autour d\'une belle piscine. Le wifi est disponible dans le lieu d\'accueil. Seule la qualité des mets est …'),
(4, 3, 1, '2', '4', '2', '434372.00', b'1', b'0', 'Magnifique séjour fin octobre ! Chambres confortables (les lodges de luxe vue mer méritent le détour). Le restaurant réserve des surprises quotidiennes, de quoi stimuler vos papilles et satisfaire tous les goûts. Mention spéciale pour le petit-déjeuner, tellement parfait ! Bravo à tout le personnel si attentionné et souriant !'),
(5, 2, 2, '4', '8', '5', '966710.00', b'1', b'1', 'Rien à redire sur les chambres, parfait.\r\nPar contre au niveau de restauration, j\'aurais voulu que les gambas soient frais, que la canapota soit moins grasse (beaucoup trop d\'huile et de sauces) et qu\'on sente réellement les ingrédients du plat et non l\'huile.\r\nMême remarque pour le poulet à l\'Asiatique.\r\nBref, Cuisine beaucoup trop grasse\r\nMais chambres excellentes');

-- --------------------------------------------------------

--
-- Structure de la table `room_category`
--

CREATE TABLE `room_category` (
  `room_category_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `room_category`
--

INSERT INTO `room_category` (`room_category_id`, `name`) VALUES
(1, 'Chambre simple: SGL'),
(2, 'bungalow'),
(3, 'Suites');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Index pour la table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`booking_id`),
  ADD KEY `fk_booking_offer_idx` (`offer_id`),
  ADD KEY `fk_booking_client_idx` (`client_id`),
  ADD KEY `fk_booking_motel_idx` (`motel_id`);

--
-- Index pour la table `car`
--
ALTER TABLE `car`
  ADD PRIMARY KEY (`car_id`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`client_id`);

--
-- Index pour la table `experience`
--
ALTER TABLE `experience`
  ADD PRIMARY KEY (`experience_id`),
  ADD KEY `fk_exp_category_idx` (`category_id`),
  ADD KEY `fk_exp_category_idx1` (`car_id`),
  ADD KEY `fk_exp_guide1_idx` (`first_guide`),
  ADD KEY `fk_exp_guide2_idx` (`second_guide_id`);

--
-- Index pour la table `experience_category`
--
ALTER TABLE `experience_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Index pour la table `experience_reservation`
--
ALTER TABLE `experience_reservation`
  ADD PRIMARY KEY (`experience_reservation_id`),
  ADD KEY `fk_exp_client_idx` (`client_id`),
  ADD KEY `fk_reserv_experience_idx` (`experience_id`);

--
-- Index pour la table `guide`
--
ALTER TABLE `guide`
  ADD PRIMARY KEY (`guide_id`);

--
-- Index pour la table `motel`
--
ALTER TABLE `motel`
  ADD PRIMARY KEY (`motel_id`);

--
-- Index pour la table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`offer_id`),
  ADD KEY `fk_offer_motel_idx` (`motel_id`),
  ADD KEY `fk_offer_roomcat_idx` (`room_category_id`);

--
-- Index pour la table `room_category`
--
ALTER TABLE `room_category`
  ADD PRIMARY KEY (`room_category_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `booking`
--
ALTER TABLE `booking`
  MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `experience`
--
ALTER TABLE `experience`
  MODIFY `experience_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `experience_category`
--
ALTER TABLE `experience_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `experience_reservation`
--
ALTER TABLE `experience_reservation`
  MODIFY `experience_reservation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `guide`
--
ALTER TABLE `guide`
  MODIFY `guide_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `motel`
--
ALTER TABLE `motel`
  MODIFY `motel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `offer`
--
ALTER TABLE `offer`
  MODIFY `offer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `room_category`
--
ALTER TABLE `room_category`
  MODIFY `room_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `fk_booking_client` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`),
  ADD CONSTRAINT `fk_booking_motel` FOREIGN KEY (`motel_id`) REFERENCES `motel` (`motel_id`),
  ADD CONSTRAINT `fk_booking_offer` FOREIGN KEY (`offer_id`) REFERENCES `offer` (`offer_id`);

--
-- Contraintes pour la table `experience`
--
ALTER TABLE `experience`
  ADD CONSTRAINT `fk_exp_car` FOREIGN KEY (`car_id`) REFERENCES `car` (`car_id`),
  ADD CONSTRAINT `fk_exp_category` FOREIGN KEY (`category_id`) REFERENCES `experience_category` (`category_id`),
  ADD CONSTRAINT `fk_exp_guide1` FOREIGN KEY (`first_guide`) REFERENCES `guide` (`guide_id`),
  ADD CONSTRAINT `fk_exp_guide2` FOREIGN KEY (`second_guide_id`) REFERENCES `guide` (`guide_id`);

--
-- Contraintes pour la table `experience_reservation`
--
ALTER TABLE `experience_reservation`
  ADD CONSTRAINT `fk_exp_client` FOREIGN KEY (`client_id`) REFERENCES `client` (`client_id`),
  ADD CONSTRAINT `fk_reserv_experience` FOREIGN KEY (`experience_id`) REFERENCES `experience` (`experience_id`);

--
-- Contraintes pour la table `offer`
--
ALTER TABLE `offer`
  ADD CONSTRAINT `fk_offer_motel` FOREIGN KEY (`motel_id`) REFERENCES `motel` (`motel_id`),
  ADD CONSTRAINT `fk_offer_roomcat` FOREIGN KEY (`room_category_id`) REFERENCES `room_category` (`room_category_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
