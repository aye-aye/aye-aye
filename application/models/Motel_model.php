<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Motel_model extends CI_Model
{
  public function template()
  {
    $this->load->view('template_motel');
  }
  // Fonction mamerina ny motel rehetra
  public function get_Motels() : array
  {
    $i = 0;
    $datas = array();
    $query = $this->db->query('SELECT * from motel');
    foreach ($query -> result_array() as $value)
    {
      $datas[$i] = $value;
      $i++;
    }
    return $datas;
  }
  // Fonction mamerina ny motel arakarakin'ny motel_id
  public function get_Motel_By_Id($id) : array
  {
    $datas = array();
    $query = $this->db->query('SELECT * from motel WHERE motel_id= '.$id.'');
    foreach ($query-> result_array() as $value)
    {
      $datas = $value;
    }
    return $datas;
  }

  /**
   * Fonction mamerina ny motel ho atao suggestion
   * @return array resultats 3 hotels random
   */
  public function get_Motel_Suggested() : array{
    $res = array();
    $query = $this->db->query("SELECT * FROM motel ORDER BY RAND() LIMIT 3");
    foreach ($query -> result_array() as $value)
    {
      $res = $value;
    }
    return $query->result_array();
  }

  /**
     * Fonction recherche
     * @param string $recherche
     * @return array resultats.
     */
  public function search($recherche) : array
  {
    $query=$this->db->query("SELECT * FROM motel WHERE name LIKE '%".$recherche."%' OR location LIKE '%".$recherche."%'");
    return $query->result_array();
  }
  /**
    *Fonction maka ny locations rehetra misy motel
    *@return string array
  */
  public function get_Locations_availables() : array
  {
    $res = array();
    $query = $this->db->query("SELECT * FROM motel");
    foreach ($query->result_array() as $value)
    {
      array_push($res, $value['location']);
    }
    return $res;
  }


  /**
     * Fonction creation et insertion de donnees dans motel ,
     * @param string $formArray

     * @return array insert.
     */
    public function create($formArray)
    {
      $this->db->insert("motel",$formArray); // equivaut à INSERT INTO motel (name,phone,email,bank_account,password,location) VALUES (?,?,?,?,?,?);
    }

     /**
     * Fonction liste de tous les motels /
     * @param string

     * @return array result_array().
     */
    public function all()
    {
      $query = $this->db->get('motel');
      if($query->num_rows() > 0){
          return $query->result();
      }else{
        return false;
      }
    }

    /**
     * Fonction mamerina motel arakarakin'ny motel_id /
     * @param string $motelId

     * @return array row_array().
     */
    public function getMotel($motelId)
    {
      $this->db->where('motel_id',$motelId);
      return $motel = $this->db->get('motel')->row_array(); //equivaut à SELECT * FROM motel WHERE motel_id= ? ;
    }

    /**
     * Fonction modification hoan'ny motel iray(1) /
     * @param string $motelId,$formArray

     * @return array result_array().
     */
    public function UpdateMotel($motelId,$formArray)
    {
      $this->db->where('motel_id',$motelId);
      $this->db->update('motel',$formArray); // UPDATE motel SET name = ? , phone = ? , email=? , bank_account = ? , password = ? , location = ?  WHERE motel_id = ? ;
    }

    /**
     * Fonction pour supprimer un motel /
     * @param string $motelId

     * @return array result_array().
     */
    public function deleteMotel($motelId)
    {
      $this->db->where('motel_id',$motelId);
      $this->db->delete('offer');
      $this->db->where('motel_id',$motelId);
      $this->db->delete('booking');
      $this->db->where('motel_id',$motelId);
      $this->db->delete('motel');
    }

}
?>
