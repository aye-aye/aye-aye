<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Admin<br>
 * Controller du table Admin
 */
class Admin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model', 'admin');
    }
    /**
     * @return void
     */
    public function index()
    {
        $this->load->view('backoffice/login_admin');
    }
    public function login()
	{
		
		if($this->admin->isValidLogin($this->input->post('login'), $this->input->post('password'))) {
            $this->session->set_userdata('admin', $this->input->post('login'));
            redirect('experience');
        } else {
            $data['error'] = true;
            $this->load->view('backoffice/login_admin', $data);
        }
	}
    // Syntaxe "TSY MAINTSY HARAHINA REHEFA MAMORONA METHODE"
    /**
     * Manao somme nombre 2
     * @param int $a nombre voalohany
     * @param int $b nombre faharoa
     *
     * @return float somme an'ny $a sy $b
     */
    public function add_number($a = 0, $b = 0) : float
    {
        return $a + $b;
    }

    /**
     * Exemple sythaxe
     * @return void
     */
    public function synthax_example() : void
    {
        // Ny miteraka bug betsaka indrindra any amin'ny vue
        if(true === true) {
            $c = 1;
            while($c <= 10) {
                $tab = ['a', 1, array()];
                  foreach($tab as $t) {
                    for($i = 0; $i != 2; $i++) {
                        echo $t;
                    }
                }
                $c++;
            }
        }

        // Sythaxe ampiasaina
        if(true === true) :
            $c = 1;
            while($c <= 10) :
                $tab = ['a', 1, array()];
                  foreach($tab as $t) :
                    for($i = 0; $i != 2; $i++) :
                        echo $t;
                    endfor;
                  endforeach;
                $c++;
            endwhile;
        endif;
    }


}
