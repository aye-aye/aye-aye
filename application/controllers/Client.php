<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Client<br>
 * Controller du table client
 */
class Client extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('client_model', 'client');
    }
   function index(){
        $this->load->model('Client_model');
        $data['clients'] = $this->Client_model->all();
        $data['page'] = $this->load->view('backoffice/list_client',$data, true);
        $data['page_title'] = 'Utilisateurs';
        $this->load->view('backoffice/template',$data);
    }
    function delete($userId){
        $user = $this->client->getClient($userId);
        if(empty($user)){
            $this->session->set_flashdata('failure','not found in database');
            redirect(base_url().'index.php/client/index');
        }
        $this->client->delete($userId);
        $this->session->set_flashdata('success','deleted successfully');
        redirect(base_url().'index.php/client/index');
    }
}
?>