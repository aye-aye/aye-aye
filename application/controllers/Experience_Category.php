<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Experience_Category<br>
 * Controller du table Experience_Category
 */
class Experience_Category extends CI_Controller
{

    function __construct() {
        parent::__construct();
        $this->load->model('experience_category_model', 'exp_category');
    }
    
    function showAll(){
        $query=  $this->exp_category->showAll();
        if($query){
            $result['categories']  = $this->exp_category->showAll();
        }
        echo json_encode($result);
    }
}