<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Car<br>
 * Controller du table Car
 */
class Car extends CI_Controller
{

    function __construct() {
        parent::__construct();
        $this->load->model('car_model', 'car');
    }
    function showAll(){
        $query=  $this->car->showAll();
        if($query){
            $result['cars']  = $this->car->showAll();
        }
        echo json_encode($result);
    }
}