<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('motel_model','motel');
		$this->load->model('experience_model','exp');
		$suggestion = array();

		$suggestion['experiences'] = $this->exp->getExperienceRandom();
		$suggestion['suggestions'] = $this->motel->get_Motel_Suggested();
		$suggestion['popularities'] = $this->exp->getPopularExperiences();
		$this->load->view('home',$suggestion);

	}
// XXX
	public function search(){
		$data = array();
		$search = $this->input->get('search');
		$data['search'] = $search;
		$data['results'] = $this->motel->search($search);
		$this->load->view('results_search',$data);
	}
}
