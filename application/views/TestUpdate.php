<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Test1</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
</head>
<body>

<div class="container">
    <h1>Modification</h1>
    <form action="<?php echo base_url('Offer/modif/');?>" method="POST">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Room Categorie</label>
                    <select name="room_category_id" class="form-control">
                        <?php foreach($room as $listeCategrie){ ?>
                            <option value="<?php echo $listeCategrie['room_category_id'];?>"><?php echo $listeCategrie['name'];?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Max adult</label>
                    <input type="number" name="max_adult" valule="" placeholder="max_adult" class="form-control">
                </div>
                <div class="form-group">
                    <label>Max Child</label>
                    <input type="number" name="max_child" valule="" placeholder="max_child" class="form-control">
                </div>
                <div class="form-group">
                    <label> Max Baby</label>
                    <input type="number" name="max_baby" valule="" placeholder="max_baby" class="form-control">
                </div>    
                <div class="form-group">
                    <label>Price</label>
                    <input type="number" name="price" valule="" placeholder="price" class="form-control">    
                </div>
                <div class="form-group">
                    <label>Have kitchen</label>
                    <select name="have_kitchen" class="form-control">
                        <option value="0">have_kitchen</option>
                        <option value="1">haven't_kitchen</option>
                    </select>    
                </div>
                <div class="form-group">
                    <label>Is available</label>
                    <select name="is_available" class="form-control">
                        <option value="0">is_available</option>
                        <option value="1">isn't_available</option>
                    </select>
                </div>    
                <div class="form-group">
                    <label>Description</label>
                    <input type="texte" name="description" value="" placeholder="description" class="form-control"/>
                </div>
            </div>
        </div>
        <button class="btn btn-primary"name="offer_id" value="<?php echo $id;?>">Update</button>
    </form>
</div>

</body>
</html>