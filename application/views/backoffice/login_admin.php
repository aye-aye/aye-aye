<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= css_url('bootstrap.min')?>">

    <title>Administrateur</title>
</head>
<body>
    <div class="container" style="margin-top: 200px;">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
            <form action="<?= site_url('admin/login') ?>" method="POST" role="form" style=" border: 1px solid green; border-radius: 3px;padding: 20px;">
                <legend style="text-align: center;">Administrateur</legend>
                <div class="form-group">
                    <label for="login">Login</label>
                    <input type="text" class="form-control" id="login" name="login" required autofocus>
                </div>
                <div class="form-group">
                    <label for="password">Mot de passe</label>
                    <input type="password" class="form-control" id="password" name="password" required>
                </div>
                <?php if (isset($error)) : ?>
                <small class="text-danger">Le login ou le mot de passe n'est pas correcte</small><br>
                <?php endif ?>
                <button type="submit" class="btn btn-success">Submit</button>
            </form>  
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
    <script src="<?= js_url('jquery.min')?>"></script>
</body>
</html>