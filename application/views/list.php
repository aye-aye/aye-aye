<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  /*
    PAGE LISTE de MOTEL
  */
 ?>
 <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Holiday</title>
  <link rel ="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/boostrap.min.css';?>">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php
            $success= $this->session->moteldata('success');
            if($success != ""){
            ?>
            <div class="alert alert-success"><?php echo $success;?></div>
           <?php
            }
            ?>
            <?php
            $failure= $this->session->moteldata('failure');
            if($failure != ""){
            ?>
            <div class="alert alert-success"><?php echo $failure;?></div>
           <?php
            }
            ?>
        </div>
</div>

<div clas="row">
    <div class="col-md-8">
        <div clas="row">
            <div class="col-6"><h2>View motel</h2> </div>
            <div class="col-6 text-right">
                <a href="<?php echo base_url().'index.php/Motel/create';?>" class="btn btn-primary">Create</a>
            </div>
        </div>
    </div>
</div>

<div class="row">
        <div class="col-md-8">
            <div class="table table-striped">
                <tr>
<th>ID</th>
<th>Name</th>
<th>Email</th>
<th>phone</th>
<th>Bank_account</th>
<th>location</th>
<th>Edit</th>
<th>Delete</th>
</tr>
<?php if(!empty($motels)) {  foreach($motels as $motel) {?>
<tr>
<td><?php echo $motel['motel_id'] ?></td>
<td><?php echo $motel['name'] ?></td>
<td><?php echo $motel['phonel'] ?></td>
<td><?php echo $motel['email'] ?></td>
<td><?php echo $motel['bank_account'] ?></td>
<td><?php echo $motel['location'] ?></td>
<td>
    <a href="<?php echo base_url().'index.php/Motel/edit/'.$motel['motel_id'] ?>" class="btn btn-primary">Edit</a>
</td>
<td>
    <a href="<?php echo base_url().'index.php/Motel/delete/'.$motel['motel_id'] ?>" class="btn btn-danger">Delete</a>
</td>
</tr>
<?php } } else {?>
<tr>
    <td colspan="5">records not found</td>
</tr>
<?php } ?>
</div>
</div>
</div>
</div>
</body>
</html>
