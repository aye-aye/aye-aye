<!DOCTYPE html>
<html>
	<head>
		<title>Crud pour controller liste client</title>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css'; ?>">
	</head>
	<body>
	<div class="navbar navbar-dark bg-dark">
		<div class="container">
			<a href="#" class="navbar-brand">Crud application</a>
		</div>
	</div>
	<div class="container" style="padding-top:10px;">
		<h3>Create User</h3>
		<hr>
		<form method="post" name="createUser" action="<?php echo site_url('client/create');?>">
		<div class="row">			
				<div class="col-md-6">
					<div class="form-group">
						<label>First Name</label>
						<input type="text" name="first_name" value="<?php echo set_value('first_name'); ?>" class="form-control">
						<?php echo form_error('first_name');?>
					</div>
					<div class="form-group">
						<label>Last Name</label>
						<input type="text" name="last_name" value="<?php echo set_value('last_name'); ?>" class="form-control">
						<?php echo form_error('last_name');?>
					<div class="form-group">
						<label>Date of birth</label>
						<input type="date" name="dob" value="<?php echo set_value('dob'); ?>" class="form-control">
						<?php echo form_error('dob');?>
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="email" value="<?php echo set_value('email'); ?>" class="form-control">
						<?php echo form_error('email');?>
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" value="password" class="form-control">
						<?php echo form_error('password');?>
					</div>
						<button class="btn btn-primary">Create</button>
						<a href="<?php echo base_url().'index.php/client/index'; ?>" class="btn-secondary btn">Cancel</a>
					</div>
				</div>
		</div>
		</form>
	</div>
	</body>
</html>
