<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  /*
    PAGE TEST AN'NY RECHERCHE MULTI CRITERE
  */

 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Search offer page</title>
  </head>
  <body>
    <form action="<?php echo site_url('offer/search_offer'); ?>" method="get">
      <p>
        <strong>Location</strong>
        <input type="text" name="place">
      </p>

      <p>
        <strong>Nombre d'adultes : </strong>
        <input type="text" name="adulte">
      </p>

      <p>
        <strong>Nombre d'enfants</strong>
        <input type="text" name="enfant">
      </p>

      <p>
        <strong>Nombre de bébés</strong>
        <input type="text" name="bebe">
      </p>

      <p>
        <strong>Cuisine</strong>
        <select name="cuisine">
          <option value="none"></option>
          <option value="1">yes</option>
          <option value="0">no</option>
        </select>
      </p>
      <button type="submit">Submit</button>
    </form>
  </body>
</html>
