<!DOCTYPE html>
<html>
	<head>
		<title>Crud pour controller update client</title>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css'; ?>">
	</head>
	<body>
	<div class="navbar navbar-dark bg-dark">
		<div class="container">
			<a href="#" class="navbar-brand">Crud application</a>
		</div>
	</div>
	<div class="container" style="padding-top:10px;">
    <div class="col-md-12">
                <?php
                $success = $this->session->userdata('success');
                if($success != "") {
                ?>
                <div class="alert alert-success"><?php echo $success;?></div>
                <?php 
                }
                ?>
                <?php
                $failure = $this->session->userdata('failure');
                if($failure != "") {
                ?>
                <div class="alert alert-success"><?php echo $failure;?></div>
                <?php 
                }
                ?>
            </div>
		<h3>Update User</h3>
		<hr>
		<form method="post" name="createUser" action="<?php echo base_url().'index.php/client/edit'.$user['client_id'];?>">
		<div class="row">			
				<div class="col-md-6">
					<div class="form-group">
						<label>First Name</label>
						<input type="text" name="first_name" value="<?php echo set_value('first_name',$user['first_name']); ?>" class="form-control">
						<?php //echo form_error('email');?>
					</div>
					<div class="form-group">
						<label>Last Name</label>
						<input type="text" name="last_name" value="<?php echo set_value('last_name',$user['last_name']); ?>" class="form-control">
					<div class="form-group">
						<label>Date of birth</label>
						<input type="date" name="dob" value="<?php echo set_value('dob',$user['birthday']); ?>" class="form-control">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="text" name="email" value="<?php echo set_value('email',$user['email']); ?>" class="form-control">
						<?php //echo form_error('email');?>
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" name="password" value="<?php echo set_value('password',$user['password']); ?>" class="form-control">
						<?php //echo form_error('password');?>
					</div>
						<button class="btn btn-primary">Update</button>
						<a href="<?php echo base_url().'index.php/client/index'; ?>" class="btn-secondary btn">Cancel</a>
					</div>
				</div>
		</div>
		</form>
	</div>
	</body>
</html>
