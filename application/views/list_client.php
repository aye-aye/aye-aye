<!DOCTYPE html>
<html>
	<head>
		<title>View pour controller liste client</title>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.min.css'; ?>">
	</head>
	<body>
	<div class="navbar navbar-dark bg-dark">
		<div class="container">
			<a href="#" class="navbar-brand">Crud application</a>
		</div>
	</div>
	<div class="container" style="padding-top:10px;">
        <div class="row">
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-6"><h3>View User</h3></div>
                    <div class="col-6">
                        <a href="<?php echo site_url('client/index') ;?>" class="btn btn-primary">Create</a>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <table class="table table-striped">
                    <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Email</th>
                       <th width="60">Edit</th>
                       <th width="100">Delete</th>
                   </tr>
                   <?php if(!empty($users)) { foreach($users as $user) { ?>
                   <tr>
                       <td><?php echo $user['client_id'] ?></td>
                       <td><?php echo $user['first_name'] ?></td>
                       <td><?php echo $user['email'] ?></td>
                       <td><a href="<?php echo site_url('client/edit').$user['client_id']?>" class="btn btn-primary">Edit</a></td>
                       <td><a href="<?php echo site_url('client/delete').$user['client_id']?>" class="btn btn-danger">Delete</a></td>
                   </tr>
                   <?php } } else {?>
                   <tr>
                       <td colspan="5">Not found</td>
                   </tr>
                   <?php } ?>
               </table>
           </div>
       </div>
 </div>
 </body>
</html>
