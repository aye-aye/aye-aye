<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  /*
    PAGE MISY NY TEST AN'NY RESULTATS DE RECHERCHE
  */

 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Resultat de recherche</title>
  </head>
  <body>
    <h2>Exemple de boucle pour la liste des offres recherchées</h2>
    <?php
      $offerSize = sizeof($offer);
      if ($offerSize<=0)
      {
        echo "Il n'y a aucun résultat correspondant à votre recherche.";
      }
      else
      {
        for ($i=0; $i<$offerSize; $i++)
        {
          echo 'Offre numero : '.$offer[$i]['offer_id'].'';
          echo '---- Prix : '.$offer[$i]['price'].' Ar';
          echo '---- Nom du motel : '.$motel[$i]['name'].'';
          echo '---- Room category : '.$room_category[$i]['name'].'';
          echo '---- Nombre d\'adultes max : '.$offer[$i]['max_adult'].'';
          echo '---- Location : '.$motel[$i]['location'].' ';
          // XXX Lien vers les détails de l'offre
          echo "<br>";
        }
      }
     ?>
  </body>
</html>
