<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <title>AYE-AYE</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo assets_url('offre_assets/vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
<!--

TemplateMo 546 Sixteen Clothing

https://templatemo.com/tm-546-sixteen-clothing

-->

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="<?php echo assets_url('offre_assets/assets/css/fontawesome.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('offre_assets/assets/css/templatemo-sixteen.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('offre_assets/assets/css/owl.css') ?>">

  </head>

  <body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->

    <!-- Header -->
    <header class="">
      <nav class="navbar navbar-expand-lg">
        <div class="container">
          <a class="navbar-brand" href="index.html"><h2>Aye - <em>Aye</em></h2></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('welcome'); ?>">Home
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">Our Offer</a>
              </li>

            </ul>
          </div>
        </div>
      </nav>
    </header>

    <!-- Page Content -->
    <div class="page-heading products-heading header-text">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="text-content">
              <h4>Hotel Name</h4>
              <h2>Lists of our products</h2>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="products">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="filters">
              <ul>
                  <li class="active" data-filter="*">Resultats de vos recherches</li>
              </ul>
            </div>
          </div>
          <div class="col-md-12">
            <div class="filters-content">
                <div class="row grid">
                  <?php if (isset($offer) OR sizeof($offer)>0): ?>
                    <?php $i=0; ?>
                    <?php foreach ($offer as $offer): ?>
                      <div class="col-lg-8 col-md-8 all gra">
                        <div class="product-item">
                          <a href="<?php echo site_url('offer/get_offer_details/'.$offer['offer_id'].''); ?>"><img src="<?php echo img_url_hotel($offer['motel_id']); ?>" alt=""></a>
                          <div class="down-content">
                            <a href="<?php echo site_url('offer/get_offer_details/'.$offer['offer_id'].''); ?>"><h4><?php echo $motel[$i]['name']; ?></h4></a>
                            <h6>Ar <?php echo $offer['price']; ?></h6>
                            <p><?php //echo $offer['description']; ?></p>
                            <ul class="stars">
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                            </ul>
                            <span>Reviews (12)</span>
                          </div>
                        </div>
                      </div>
                    <?php $i++;  ?>
                    
                    <?php endforeach; ?>
                  <?php endif; ?>
                  <?php if (!isset($offer) OR sizeof($offer)<=0): ?>
                    <div class="col-lg-10 col-md-1 all gra">
                      <h4>Aucun résultat correspondant à votre recherche n'a été trouvé</h4>
                    </div>
                  <?php endif; ?>
                </div>
            </div>
          </div>
          <!--
          <div class="col-md-12">
            <ul class="pages">
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
          </div>
          -->
        </div>
      </div>
    </div>


    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="inner-content">
              <p>Copyright &copy; 2020 Sixteen Clothing Co., Ltd.

            - Design: <a rel="nofollow noopener" href="https://templatemo.com" target="_blank">TemplateMo</a></p>
            </div>
          </div>
        </div>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo assets_url('offre_assets/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src=<?php echo assets_url('offre_assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>""></script>


    <!-- Additional Scripts -->
    <script src="<?php echo assets_url('offre_assets/assets/js/custom.js'); ?>"></script>
    <script src="<?php echo assets_url('offre_assets/assets/js/owl.js'); ?>"></script>
    <script src="<?php echo assets_url('offre_assets/assets/js/slick.js'); ?>"></script>
    <script src="<?php echo assets_url('offre_assets/assets/js/isotope.js'); ?>"></script>
    <script src="<?php echo assets_url('offre_assets/assets/js/accordions.js'); ?>"></script>

    <script language = "text/Javascript">
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>
  </body>
</html>
