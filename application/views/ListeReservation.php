<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>TestTemplate</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
</head>
<body>

<div id="container">
	<form action="<?php echo base_url('Offer/delete/');?>" method="POST">
		<table width="400" class="table">
      		<thead class="thead-dark">
      			<tr>
            		<th scope="col">Booking_id</th>
            		<th scope="col">Client_first_name</th>
            		<th scope="col">Motel_name</th>
          		</tr>
          	</thead>
          	<tbody>
          		<tr>
          			<?php foreach($listeRes as $listeReservation){ ?>
              			<td><?php echo $listeReservation['booking_id']; ?></td>
              			<td><?php echo $listeReservation['first_name']; ?></td>
              			<td><?php echo $listeReservation['name']; ?></td>
              		<?php } ?>
              	</tr>
            </tbody>
		</table>
   </form>
   <form action="<?php echo base_url('Offer/retour/');?>" method="POST">
        <button class="btn btn-primary">retour</button>
    </form>
</div>

</body>
</html>