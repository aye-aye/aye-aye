<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Aye-Aye</title>
  <?php //echo assets_url('home_assets'); ?>
<!--
Holiday Template
http://www.templatemo.com/tm-475-holiday
-->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700' rel='stylesheet' type='text/css'>
  <link href=<?php echo assets_url("home_assets/css/font-awesome.min.css"); ?> rel="stylesheet">
  <link href=<?php echo assets_url("home_assets/css/bootstrap.min.css"); ?> rel="stylesheet">
  <link href=<?php echo assets_url("home_assets/css/bootstrap-datetimepicker.min.css"); ?> rel="stylesheet">
  <link href=<?php echo assets_url("home_assets/css/flexslider.css"); ?> rel="stylesheet">
  <link href=<?php echo assets_url("home_assets/css/templatemo-style.css"); ?> rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body class="tm-gray-bg">
  	<!-- Header -->
    <div class="tm-header">
  		<div class="container">
  			<div class="row">
  				<div class="col-lg-2 col-md-2 col-sm-3 tm-site-name-container">
  					<a href="#" class="tm-site-name">Holiday</a>
  				</div>
          <!--
          <form method="get" action="<?php //echo site_url('welcome/search') ?>">
						<input placeholder="Recherche" class="tm-site-name" name="search" style="border-radius:10px;">
					</form>
          -->
				<div class="col-lg-4 col-md-2 col-sm-3 tm-site-name-container">

  				</div>
	  			<div class="col-lg-6 col-md-8 col-sm-9">
	  				<div class="mobile-menu-icon">
		              <i class="fa fa-bars"></i>
		            </div>
	  				<nav class="tm-nav">
						<ul>
							<li><a href="<?php echo site_url('client/display_login') ?>">Se connecter</a></li>
							<li><a href="<?php echo site_url('motel/display_login') ?>">Hôtel</a></li>
							<li><a href="<?php echo site_url('Experience/list') ?>">Nos experiences</a></li>
						</ul>
					</nav>
	  			</div>
  			</div>
  		</div>
  	</div>

	<!-- Banner -->
	<section class="tm-banner">
		<!-- Flexslider / SUGGESTION-->
		<div class="flexslider flexslider-banner">
		  <ul class="slides">
        <?php foreach($suggestions as $sug) : ?>
					<li>
						<div class="tm-banner-inner">
							<h1 class="tm-banner-title"><span class="tm-yellow-text"><?=$sug['name']?></span></h1>
							<p class="tm-banner-subtitle"><?=$sug['location']?></p>
							<a href="<?php echo site_url('offer/list_targetted?id='.$sug['motel_id'].'') ?>" class="tm-banner-link">Voir les offres pour celui-là</a>
						</div>
						<img src="<?php echo img_url_hotel(''.$sug['motel_id'].'');?>" alt="Image" />
					</li>
				<?php endforeach ?>

		  </ul>
		</div>
	</section>

	<!-- gray bg -->
	<section class="container tm-home-section-1" id="more">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6">
				<!-- Nav tabs -->
				<div class="tm-home-box-1">
					<ul class="nav nav-tabs tm-white-bg" role="tablist" id="hotelCarTabs">
					    <li role="presentation" class="active">
					    	<a href="#hotel" aria-controls="hotel" role="tab" data-toggle="tab">Hotel</a>
					    </li>
					    <li role="presentation">
					    	<a href="#car" aria-controls="car" role="tab" data-toggle="tab">Car Rental</a>
					    </li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
					    <div role="tabpanel" class="tab-pane fade in active tm-white-bg" id="hotel">
					    	<div class="tm-search-box effect2">
								<form action="<?php echo site_url(); ?>offer/search_offer" method="get" class="hotel-search-form">
									<div class="tm-form-inner">
										<div class="form-group">
                      <input type="text" class="form-control" name="place" placeholder="Place">
							      </div>
                    <!--
                    <div class="form-group">
                              <div class='input-group date' id='datetimepicker1'>
                                  <input type='text' class="form-control" placeholder="Check-in Date" />
                                  <span class="input-group-addon">
                                      <span class="fa fa-calendar"></span>
                                  </span>
                              </div>
                    </div>
                    <div class="form-group">
                              <div class='input-group date' id='datetimepicker2'>
                                  <input type='text' class="form-control" placeholder="Check-out Date" />
                                  <span class="input-group-addon">
                                      <span class="fa fa-calendar"></span>
                                  </span>
                              </div>
                   </div>
                    -->
                  <div class="form-group">
                    <select class="form-control" name="adulte">
                           <option value="">-- Nombre d'adultes -- </option>
                           <?php for ($i=0; $i <= 10; $i++) {
                             ?>
                              <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                             <?php
                           } ?>
                    </select>

                  </div>
                  <div class="form-group">
                    <select class="form-control" name="enfant">
                           <option value="">-- Nombre d'enfants -- </option>
                           <?php for ($i=0; $i <= 10; $i++) {
                             ?>
                              <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                             <?php
                           } ?>
                    </select>
                  </div>
							    <div class="form-group">
							         <select class="form-control" name="bebe">
							            	 	<option value="">-- Nombre de bébés --</option>
                              <?php for ($i=0; $i <= 10; $i++) {
                                ?>
                                 <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                              } ?>
											</select>
							   </div>
                 <div class="form-group margin-bottom-0">
                   <input id="cuisine1" type="checkbox" name="cuisine" value="1">
                   <label for="cuisine1">Avec cuisine</label>
                 </div>
									</div>
						            <div class="form-group tm-yellow-gradient-bg text-center">
						            	<button type="submit" name="submit" class="tm-yellow-btn">Check Now</button>
						            </div>
								</form>
							</div>
					    </div>
					    <div role="tabpanel" class="tab-pane fade tm-white-bg" id="car">
							<div class="tm-search-box effect2">
								<form action="#" method="post" class="hotel-search-form">
									<div class="tm-form-inner">
										<div class="form-group">
							            	 <select class="form-control">
							            	 	<option value="">-- Select Model -- </option>
							            	 	<option value="shangrila">BMW</option>
												<option value="chatrium">Mercedes-Benz</option>
												<option value="fourseasons">Toyota</option>
												<option value="hilton">Honda</option>
											</select>
							          	</div>
							          	<div class="form-group">
							                <div class='input-group date-time' id='datetimepicker3'>
							                    <input type='text' class="form-control" placeholder="Pickup Date" />
							                    <span class="input-group-addon">
							                        <span class="fa fa-calendar"></span>
							                    </span>
							                </div>
							            </div>
							          	<div class="form-group">
							                <div class='input-group date-time' id='datetimepicker4'>
							                    <input type='text' class="form-control" placeholder="Return Date" />
							                    <span class="input-group-addon">
							                        <span class="fa fa-calendar"></span>
							                    </span>
							                </div>
							            </div>
							            <div class="form-group">
							            	 <select class="form-control">
							            	 	<option value="">-- Options -- </option>
							            	 	<option value="">Child Seat</option>
												<option value="">GPS Navigator</option>
												<option value="">Insurance</option>
											</select>
							          	</div>
									</div>
						            <div class="form-group tm-yellow-gradient-bg text-center">
						            	<button type="submit" name="submit" class="tm-yellow-btn">Check Now</button>
						            </div>
								</form>
							</div>
					    </div>
					</div>
				</div>
			</div>

			<!-- Suggestion ou top 1 reservation hotel -->
      <div class="col-lg-4 col-md-4 col-sm-6">
        <div class="tm-home-box-1 tm-home-box-1-2 tm-home-box-1-center">
          <img src="<?php echo img_url_hotel('2');?>" alt="image" class="img-responsive" height="600" width="346">
          <a href="<?=site_url('offer/search_offer')?>">
            <div class="tm-green-gradient-bg tm-city-price-container">

              <span>Liste des hôtels</span>

            </div>
          </a>
        </div>
      </div>
			<!-- Fin suggestion ou top 1 pour hotel -->
			<!-- Suggestion ou top 1 reservation experience -->
      <div class="col-lg-4 col-md-4 col-sm-6">
       <div class="tm-home-box-1 tm-home-box-1-2 tm-home-box-1-right">
         <img src="<?php echo img_url_exp('2');?>" alt="image" class="img-responsive" height="600" width="346">
         <a href="<?=site_url('experience/list')?>">
           <div class="tm-red-gradient-bg tm-city-price-container">

             <span>Liste des experiences</span>

           </div>
         </a>
       </div>
     </div>
			<!-- Fin suggestion ou top 1 pour experience -->
		</div>

<<<<<<< HEAD
		<div class="section-margin-top">
			<div class="row">
				<div class="tm-section-header">
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
					<div class="col-lg-6 col-md-6 col-sm-6"><h2 class="tm-section-title">Random Experience</h2></div>
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
				</div>
			</div>
			<!-- 4 Random experience -->
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
						<div class="tm-home-box-2">
							<img src="img/index-03.jpg" alt="image" class="img-responsive">
							<h3>Proin Gravida Nibhvel Lorem Quis Bind</h3>
							<p class="tm-date">28 March 2016</p>
							<div class="tm-home-box-2-container">
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
								<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description">Travel</span></a>
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
						<div class="tm-home-box-2">
							<img src="img/index-04.jpg" alt="image" class="img-responsive">
							<h3>Proin Gravida Nibhvel Lorem Quis Bind</h3>
							<p class="tm-date">26 March 2016</p>
							<div class="tm-home-box-2-container">
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
								<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description">Travel</span></a>
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
						<div class="tm-home-box-2">
							<img src="img/index-05.jpg" alt="image" class="img-responsive">
							<h3>Proin Gravida Nibhvel Lorem Quis Bind</h3>
							<p class="tm-date">24 March 2016</p>
							<div class="tm-home-box-2-container">
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
								<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description">Travel</span></a>
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
						<div class="tm-home-box-2 tm-home-box-2-right">
							<img src="img/index-06.jpg" alt="image" class="img-responsive">
							<h3>Proin Gravida Nibhvel Lorem Quis Bind</h3>
							<p class="tm-date">22 March 2016</p>
							<div class="tm-home-box-2-container">
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
								<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description">Travel</span></a>
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
							</div>
						</div>
					</div>
				</div>
			<!-- Fin Random experience -->
			<div class="row">
				<div class="col-lg-12">
					<p class="home-description">Holiday is free Bootstrap v3.3.5 responsive template for tour and travel websites. You can download and use this layout for any purpose. You do not need to provide a credit link to us. If you have any question, feel free to contact us. Credit goes to <a rel="nofollow" href="http://unsplash.com" target="_parent">Unsplash</a> for images used in this template.</p>
=======
    <div class="section-margin-top" style="margin-top: 270px;">
			<div class="row">
				<div class="tm-section-header">
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
					<div class="col-lg-6 col-md-6 col-sm-6"><h2 class="tm-section-title">Experiences Disponibles</h2></div>
					<div class="col-lg-3 col-md-3 col-sm-3"><hr></div>
				</div>
			</div>
			<div class="row">
				<?php foreach($experiences as $exp) :?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
					<div class="tm-home-box-2">
						<img src="<?php echo img_url_exp(''.$exp['experience_id'].'');?>" alt="image" class="img-responsive">
						<h3><?=$exp['name']?></h3>
						<!--<p class="tm-date"><?=$exp['description']?></p>-->
						<div class="tm-home-box-2-container">
							<!--<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>-->
							<a href="<?php echo site_url('experience/experience_details?id='.$exp['experience_id'].'') ?>" class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Plus de détails</span></a>
							<!--<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>-->
						</div>
					</div>
				</div>
				<?php endforeach ?>
				<!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
					<div class="tm-home-box-2">
					    <img src="<?php echo img_url('index-04.jpg');?>" alt="image" class="img-responsive">
						<h3>Proin Gravida Nibhvel Lorem Quis Bind</h3>
						<p class="tm-date">26 March 2016</p>
						<div class="tm-home-box-2-container">
							<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
							<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description">Travel</span></a>
							<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
					<div class="tm-home-box-2">
					    <img src="<?php echo img_url('index-05.jpg');?>" alt="image" class="img-responsive">
						<h3>Proin Gravida Nibhvel Lorem Quis Bind</h3>
						<p class="tm-date">24 March 2016</p>
						<div class="tm-home-box-2-container">
							<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
							<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description">Travel</span></a>
							<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
					<div class="tm-home-box-2 tm-home-box-2-right">
					    <img src="<?php echo img_url('index-06.jpg');?>" alt="image" class="img-responsive">
						<h3>Proin Gravida Nibhvel Lorem Quis Bind</h3>
						<p class="tm-date">22 March 2016</p>
						<div class="tm-home-box-2-container">
							<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
							<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description">Travel</span></a>
							<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
						</div>
					</div>
				</div>-->
			</div>
			<div class="row">
				<div class="col-lg-12">
					<p class="home-description">Vous pouvez directement vous rendre à la liste de toutes les experiences en cliquant <a rel="nofollow" href="<?=site_url('experience/list')?>" target="_parent">ici</a>.</p>
>>>>>>> b1029c626e8c279b89011077021243ed218f06d5
				</div>
			</div>
		</div>
	</section>

	<!-- white back-ground color -->
<<<<<<< HEAD
	<section class="tm-white-bg section-padding-bottom">
=======
  <section class="tm-white-bg section-padding-bottom">
>>>>>>> b1029c626e8c279b89011077021243ed218f06d5
		<div class="container">
			<div class="row">
				<div class="tm-section-header section-margin-top">
					<div class="col-lg-4 col-md-3 col-sm-3"><hr></div>
<<<<<<< HEAD
					<div class="col-lg-4 col-md-6 col-sm-6"><h2 class="tm-section-title">Popular Packages</h2></div>
					<div class="col-lg-4 col-md-3 col-sm-3"><hr></div>
				</div>
				<!-- Popular destination et experience -->
					<div class="col-lg-6">
						<div class="tm-home-box-3">
							<div class="tm-home-box-3-img-container">
								<img src="img/index-07.jpg" alt="image" class="img-responsive">
							</div>
							<div class="tm-home-box-3-info">
								<p class="tm-home-box-3-description">Proin gravida nibhvell velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum</p>
								<div class="tm-home-box-2-container">
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
								<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Travel</span></a>
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
							</div>
							</div>
						</div>
					 </div>
					 <div class="col-lg-6">
						<div class="tm-home-box-3">
							<div class="tm-home-box-3-img-container">
								<img src="img/index-08.jpg" alt="image" class="img-responsive">
							</div>
							<div class="tm-home-box-3-info">
								<p class="tm-home-box-3-description">Proin gravida nibhvell velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum</p>
								<div class="tm-home-box-2-container">
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
								<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Travel</span></a>
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
							</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="tm-home-box-3">
							<div class="tm-home-box-3-img-container">
								<img src="img/index-09.jpg" alt="image" class="img-responsive">
							</div>
							<div class="tm-home-box-3-info">
								<p class="tm-home-box-3-description">Proin gravida nibhvell velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum</p>
								<div class="tm-home-box-2-container">
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
								<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Travel</span></a>
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
							</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="tm-home-box-3">
							<div class="tm-home-box-3-img-container">
								<img src="img/index-10.jpg" alt="image" class="img-responsive">
							</div>
							<div class="tm-home-box-3-info">
								<p class="tm-home-box-3-description">Proin gravida nibhvell velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum</p>
								<div class="tm-home-box-2-container">
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
								<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Travel</span></a>
								<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
							</div>
							</div>
						</div>
					</div>
				<!-- Fin popular destination et experience -->
			</div>
		</div>
	</section>
	<footer class="tm-black-bg">
		<div class="container">
			<div class="row">
				<p class="tm-copyright-text">Copyright &copy; 2084 Your Company Name

                | Designed by templatemo</p>
=======
					<div class="col-lg-4 col-md-6 col-sm-6"><h2 class="tm-section-title">Destination & Experiences populaires</h2></div>
					<div class="col-lg-4 col-md-3 col-sm-3"><hr></div>
				</div>
				<?php foreach($popularities as $pop) : ?>
					<div class="col-lg-12">
						<div class="tm-home-box-3">
							<div class="tm-home-box-3-img-container">
								<img src="<?php echo img_url_exp(''.$pop['experience_id'].'');?>" alt="image" class="img-responsive">
							</div>
							<div class="tm-home-box-3-info">
								<p class="tm-home-box-3-description" style="min-height: 150px;"><?=$pop['description']?></p>
								<div class="tm-home-box-2-container">
								<!--<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>-->
								<a href="<?php echo site_url('experience/experience_details?id='.$pop['experience_id'].'') ?>" class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Plus de détails</span></a>
								<!--<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>-->
								</div>
							</div>
						</div>
					</div>
					<!--<div class="col-lg-6">
						<div class="tm-home-box-3">
							<div class="tm-home-box-3-img-container">
								<img src="<?php //echo img_url_exp(''.$pop['experience_id'].'');?>" alt="image" class="img-responsive">
							</div>
							<div class="tm-home-box-3-info">
								<p class="tm-home-box-3-description"><?=$pop['description']?></p>
								<div class="tm-home-box-2-container">
								<!--<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>-->
								<a href="<?php echo site_url('experience/experience_details?id='.$pop['experience_id'].'') ?>" class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Plus de détails</span></a>
								<!--<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
								</div>
							</div>
						</div>
					</div>
				<div class="col-lg-6">
				    <div class="tm-home-box-3">
						<div class="tm-home-box-3-img-container">
							<img src="<?php //echo img_url('index-09.jpg');?>" alt="image" class="img-responsive">
						</div>
						<div class="tm-home-box-3-info">
							<p class="tm-home-box-3-description">Proin gravida nibhvell velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum</p>
					        <div class="tm-home-box-2-container">
							<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
							<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Travel</span></a>
							<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
						</div>
						</div>
					</div>
			    </div>
			    <div class="col-lg-6">
			        <div class="tm-home-box-3">
						<div class="tm-home-box-3-img-container">
							<img src="<?php //echo img_url('index-10.jpg');?>" alt="image" class="img-responsive">
						</div>
						<div class="tm-home-box-3-info">
							<p class="tm-home-box-3-description">Proin gravida nibhvell velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum</p>
					        <div class="tm-home-box-2-container">
							<a href="#" class="tm-home-box-2-link"><i class="fa fa-heart tm-home-box-2-icon border-right"></i></a>
							<a href="#" class="tm-home-box-2-link"><span class="tm-home-box-2-description box-3">Travel</span></a>
							<a href="#" class="tm-home-box-2-link"><i class="fa fa-edit tm-home-box-2-icon border-left"></i></a>
						</div>
						</div>
					</div>
			   	</div>-->
				<?php endforeach ?>
			</div>
		</div>
	</section>
  <footer class="tm-black-bg">
		<div class="container">
			<div class="row">
				<p class="tm-copyright-text">Copyright &copy; 2021 Aye-Aye

                | Site officiel</p>
>>>>>>> b1029c626e8c279b89011077021243ed218f06d5
			</div>
		</div>
	</footer>
	<script type="text/javascript" src="<?php echo assets_url('home_assets/js/jquery-1.11.2.min.js');?>"></script>
  <script type="text/javascript" src="<?php echo assets_url('home_assets/js/moment.js'); ?>"></script>							<!-- moment.js -->
	<script type="text/javascript" src="<?php echo assets_url('home_assets/js/bootstrap.min.js'); ?>"></script>					<!-- bootstrap js -->
	<script type="text/javascript" src="<?php echo assets_url('home_assets/js/bootstrap-datetimepicker.min.js'); ?>"></script>	<!-- bootstrap date time picker js, http://eonasdan.github.io/bootstrap-datetimepicker/ -->
	<script type="text/javascript" src="<?php echo assets_url('home_assets/js/jquery.flexslider-min.js'); ?>"></script>

<!--
	<script src="js/froogaloop.js"></script>
	<script src="js/jquery.fitvid.js"></script>
-->
   	<script type="text/javascript" src="<?php echo assets_url('home_assets/js/templatemo-script.js');?>"></script>      		<!-- Templatemo Script -->
	<script>
		// HTML document is loaded. DOM is ready.
		$(function() {

			$('#hotelCarTabs a').click(function (e) {
			  e.preventDefault()
			  $(this).tab('show')
			})

        	$('.date').datetimepicker({
            	format: 'MM/DD/YYYY'
            });
            $('.date-time').datetimepicker();

			// https://css-tricks.com/snippets/jquery/smooth-scrolling/
		  	$('a[href*=#]:not([href=#])').click(function() {
			    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			      var target = $(this.hash);
			      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			      if (target.length) {
			        $('html,body').animate({
			          scrollTop: target.offset().top
			        }, 1000);
			        return false;
			      }
			    }
		  	});
		});

		// Load Flexslider when everything is loaded.
		$(window).load(function() {
			// Vimeo API nonsense

/*
			  var player = document.getElementById('player_1');
			  $f(player).addEvent('ready', ready);

			  function addEvent(element, eventName, callback) {
			    if (element.addEventListener) {
			      element.addEventListener(eventName, callback, false)
			    } else {
			      element.attachEvent(eventName, callback, false);
			    }
			  }

			  function ready(player_id) {
			    var froogaloop = $f(player_id);
			    froogaloop.addEvent('play', function(data) {
			      $('.flexslider').flexslider("pause");
			    });
			    froogaloop.addEvent('pause', function(data) {
			      $('.flexslider').flexslider("play");
			    });
			  }
*/



			  // Call fitVid before FlexSlider initializes, so the proper initial height can be retrieved.
/*

			  $(".flexslider")
			    .fitVids()
			    .flexslider({
			      animation: "slide",
			      useCSS: false,
			      animationLoop: false,
			      smoothHeight: true,
			      controlNav: false,
			      before: function(slider){
			        $f(player).api('pause');
			      }
			  });
*/




//	For images only
		    $('.flexslider').flexslider({
			    controlNav: false
		    });


	  	});
	</script>
 </body>
 </html>
