<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <title>AYE-AYE</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo assets_url('offre_assets/vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">
<!--

TemplateMo 546 Sixteen Clothing

https://templatemo.com/tm-546-sixteen-clothing

-->

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="<?php echo assets_url('offre_assets/assets/css/fontawesome.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('offre_assets/assets/css/templatemo-sixteen.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('offre_assets/assets/css/owl.css') ?>">
	<link rel="stylesheet" href="<?php echo assets_url('plugins/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('plugins/ps-icon/style.css') ?>">
    <!-- CSS Library-->
    <link rel="stylesheet" href="<?php echo assets_url('plugins/bootstrap/dist/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('plugins/owl-carousel/assets/owl.carousel.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('plugins/slick/slick/slick.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('plugins/bootstrap-select/dist/css/bootstrap-select.min.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('plugins/Magnific-Popup/dist/magnific-popup.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('plugins/jquery-ui/jquery-ui.min.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('plugins/revolution/css/settings.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('plugins/revolution/css/layers.css') ?>">
    <link rel="stylesheet" href="<?php echo assets_url('plugins/revolution/css/navigation.css') ?>">
	<link rel="stylesheet" href="<?php echo assets_url('css/style.css') ?>">
    <!-- Custom-->
    <link rel="stylesheet" href="css/style.css">
  </head>

  <body>

    <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- ***** Preloader End ***** -->

    <!-- Header -->
    <header class="">
      <nav class="navbar navbar-expand-lg">
        <div class="container">
          <a class="navbar-brand" href="index.html"><h2>Aye - <em>Aye</em></h2></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('welcome'); ?>">Home
                  <span class="sr-only">(current)</span>
                </a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="#">Our Offer</a>
              </li>
              
            </ul>
          </div>
        </div>
      </nav>
    </header>

    <!-- Page Content -->
    <div class="page-heading products-heading header-text">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="text-content">
              <h4>Hotel Name</h4>
              <h2>Lists of our products</h2>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="products">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="filters">
              <ul>
                  <li class="active" data-filter="*">Plus de détails sur l'offre du motel <?php echo $motel['name']; ?></li>
              </ul>
            </div>
          </div>
          <div class="col-md-12">
            <div class="filters-content">
                <div class="row grid">
                  <?php if (isset($offer) OR sizeof($offer)>0): ?>
                      <div class="col-lg-6 col-md-5 all gra">
                        <div class="product-item">
                          <a href="#"><img src="<?php echo img_url_hotel($offer['motel_id']); ?>" alt=""></a>
                          <div class="down-content">
                            <a href="#"><h4><?php echo $motel['name']; ?></h4></a>
                            <h6>Ar <?php echo $offer['price']; ?></h6>
                            <p><?php echo $offer['description']; ?></p>
                            <ul class="stars">
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                              <li><i class="fa fa-star"></i></li>
                            </ul>
                          </div>
                        </div>
                      </div>
					
					<!-- Confirmation -->
				    <div class="col-lg-4 col-md-10 col-sm-12 col-xs-12 all gra " style="background-color:green;">
						<form action="#" method="post">
							<h2 class="mb-4">Nombres :</h2>
								<h4>Adultes : <input type="number" name="#" style="width: 125px; height: 25px; float: right;"></h4>
								<h4>Enfants : <input type="number" name="#" style="width: 125px; height: 25px;float: right;"></h4>
								<h4>bébés : <input type="number" name="#" style="width: 125px;height: 25px; float: right;"></h4>
							<center><h2 class="mb-4">payment method </h2>
								<input type="checkbox" name = "visa"><a href="#"><img src="<?php echo img_url('payment\1.png')?>" alt=""></a></input>
								<input type="checkbox" name = "masterCard"><a href="#"><img src="<?php echo img_url('payment/2.png')?>" alt=""></a></input>
								<input type="checkbox" name = "american express" class="mb-4"><a href="#"><img src="<?php echo img_url('payment/3.png')?>" alt=""></a></input></center>
							<p><center><input type="submit" value="valider"></center></p>
						</form>							
					</div>
					<!-- Fin confirmation -->
					
                      </footer>
                  <?php elseif (!isset($offer) OR sizeof($offer)<=0): ?>
                    <div class="col-lg-10 col-md-1 all gra">
                      <h4>Aucun résultat correspondant à votre recherche n'a été trouvé</h4>
                    </div>
                  <?php endif; ?>
                </div>
            </div>
          </div>
          <!--
          <div class="col-md-12">
            <ul class="pages">
              <li class="active"><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
            </ul>
          </div>
          -->
        </div>
      </div>
    </div>


    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="inner-content">
              <p>Copyright &copy; 2020 Sixteen Clothing Co., Ltd.

            - Design: <a rel="nofollow noopener" href="https://templatemo.com" target="_blank">TemplateMo</a></p>
            </div>
          </div>
        </div>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo assets_url('offre_assets/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src=<?php echo assets_url('offre_assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>""></script>


    <!-- Additional Scripts -->
    <script src="<?php echo assets_url('offre_assets/assets/js/custom.js'); ?>"></script>
    <script src="<?php echo assets_url('offre_assets/assets/js/owl.js'); ?>"></script>
    <script src="<?php echo assets_url('offre_assets/assets/js/slick.js'); ?>"></script>
    <script src="<?php echo assets_url('offre_assets/assets/js/isotope.js'); ?>"></script>
    <script src="<?php echo assets_url('offre_assets/assets/js/accordions.js'); ?>"></script>

    <script language = "text/Javascript">
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }
    </script>
  </body>
</html>
