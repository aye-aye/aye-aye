<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>TestTemplate</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
</head>
<body>

<div id="container">
	<form action="<?php echo base_url('Offer/delete/');?>" method="POST">
		<table width="400" class="table">
      <thead class="thead-dark">
          <tr>
            <th scope="col">Offer_id</th>
            <th scope="col">Room_Category</th>
            <th scope="col">Max_Adult</th>
            <th scope="col">Max_Child</th>
            <th scope="col">Max_Baby</th>
            <th scope="col">Price</th>
            <th scope="col">Have_Kitchen</th>
            <th scope="col">Is_Available</th>
            <th scope="col">Suppresion</th>
            <th scope="col">Modification</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach($offer_id as $liste){ ?>
            <tr>
              <td><?php echo $liste['offer_id']; ?></td>
              <td><?php echo $liste['name']; ?></td>
              <td><?php echo $liste['max_adult']; ?></td>
              <td><?php echo $liste['max_child']; ?></td>
              <td><?php echo $liste['max_baby']; ?></td>
              <td><?php echo $liste['price']; ?></td>
              <td><?php echo $liste['have_kitchen']; ?></td>
              <td><?php echo $liste['is_available']; ?></td>
              <td><button class="btn btn-primary" name="offer_id" value="<?php echo $liste['offer_id'];?>" onclick="return(confirm('Etes-vous sûr de vouloir supprimer cette entrée?'));">Delete</button></td>
              <td><form action="<?php echo base_url('Offer/indexUpdate/');?>" method="POST">
                    <button class="btn btn-primary" name="id_offer" value="<?php echo $liste['offer_id'];?>">Modifier</button>
                  </form>
              </td>
            </tr>
          <?php } ?>
        </tbody>  
      </table>
   </form>
</div>

</body>
</html>