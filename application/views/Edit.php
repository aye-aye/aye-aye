<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  /*
    PAGE MOFIDICATION de MOTEL 
  */
 ?>
 <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Holiday</title>
  <link rel ="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/boostrap.min.css';?>">
</head>
<body>
<div class="container">
        <h2> Update motel</h2>
        <form method="post" name="createMotel" action="<?php echo base_url().'index.php/Motel/edit'.$motel['motel_id']?>">
        <div class="row">
            
            <div class="col-md-12">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" value="<?php echo set_value('name',$motel['name']);?>" class="form-control">
                    <?php echo form_error('name');?>
                </div>
                <div class="form-group">
                    <label>Phone</label>
                    <input type="tel" name="phone" value="<?php echo set_value('phone',$motel['phone']);?>" class="form-control">
                    <?php echo form_error('phone');?>                
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" value="<?php echo set_value('email',$motel['email']);?>" class="form-control">
                    <?php echo form_error('email');?>
                </div>
                <div class="form-group">
                    <label>bank_account</label>
                    <input type="number" name="bank_account" value="<?php echo set_value('bank_account',$motel['bank_account']);?>" class="form-control">
                    <?php echo form_error('bank_account');?>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" value="<?php echo set_value('password',$motel['password']);?>" class="form-control">
                    <?php echo form_error('password');?>
                </div>
                <div class="form-group">
                    <label>Location</label>
                    <input type="text" name="location" value="<?php echo set_value('location',$motel['location']);?>" class="form-control">
                    <?php echo form_error('location');?>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">update</button>
                    <a href="<?php echo base_url().'index.php/Motel/index';?>" class="btn btn-primary">Cancel</a>
                </div>
            </form>
        </div>
    </div>    
</div>
</body>
</html>